# Electron GUI Best Practices


### What is Electron
Electron is an open-source framework that allows for the development of desktop GUI applications using front and back end components originally developed for web applications: Node.js runtime for the back end and Chromium for the frontend




# Principles

Electron is a cross platform framework but there are principles that every app has to adhere to regardless of platform or library API 

## Principle One: Clarity

Clarity is the most important aspect of any interface. People must be able to recognize what an interface does, why they would use it and understand what they are interacting with. There is no room for confusion in a general user interface. 

Reference: https://www.invisionapp.com/blog/core-principles-of-ui-design/

## Principle Two: One Primary Action Per Screen 

It is important to implement a GUI clearly and the less that is happening at one time the less chance there is in confusing a user. One hundred clear screens is preferable over a single cluttered screen.

Reference: http://bokardo.com/principles-of-user-interface-design/

## Principle Three: Great Design is Invisible

Great design often goes unnoticed by the people who use it. One reason is if the design was successful the user can focus on their own goals and not the interface. An example in my application is that I have chosen to not add a button for the user to after choosing their wanted color from the three sliders, instead I called my javascript function using oninput="" so that dynamically the app background changes when the user.

Reference: http://blog.teamtreehouse.com/10-user-interface-design-fundamentals

## Principle Four: Interfaces Exist To Be Used

As in most design disciplines, interface design is successful when people are using what you've designed. Like a beautifully constructed path that is inconvenient to walk. Interface design is just as important as creating an interface worth using.

Reference: https://www.usability.gov/what-and-why/user-interface-design.html

## Principle Five: Conserve Attention

Attention is precious. Don't litter the sides of an application with distractions, remember why the screen exists in the first place. If somebody is ready let them finish reading before shoving things in their face. Honor attention and not only will your users be happy, your results will be better.

Reference: https://thenextweb.com/dd/2016/08/09/design-principles-behind-user-interface/

## Principle Six: Keep users in Control

Humans are most comfortable when they feel in control of themselves and their environment. Thoughtless GUI design takes away that comfort by forcing people into confusing pathways, unplanned interactions and surprising outcomes.

Reference: https://www.slideshare.net/NatnaelGonfa/five-principles-of-user-interface-design

## Principle Seven: Provide a Natural Next Step

Very few interactions are meant to be the last step, so thoughtful design allows each person a new interaction through your interface. Anticipate what the next interaction should be and design to support it.

Reference: https://www.nngroup.com/courses/hci/

## Principle Eight: Consistency Matters

Screen elements should not appear consistent with each other unless they behave consistently with each other. Elements that behave the same should look the same.

Reference: https://web.archive.org/web/20120326125547/http://chandlerproject.org/Journal/HumaneUserInterface20041102

## Principle Nine: Smart Design Reduces Cognitive Load

Smart organization of screen elements can make the many look like the few. This helps people understand your interface easier and more quickly. Group together like elements, show natural relationships by placement and orientation. By smartly designing your GUI you will lower the cognitive load on the user.

Reference: http://www.ambysoft.com/essays/userInterfaceDesign.html

## Principle Ten: The Zero State

First time experiences in a GUI is one of the most important. In order to help our users get up to speed with the design the zero state; the time when nothing has accord  yet shouldn't just be a blank canvas it should inform the user on where to progress next and what the general function of your design and application is.

Reference: http://www.designprinciplesftw.com/collections/10-usability-heuristics-for-user-interface-design

-------------------------------------------------------------------------------------------------------------------

<A well written report - all of your listed principles were covered in your app. - Mike T.>